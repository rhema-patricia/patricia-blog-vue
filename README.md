# Patricia Blog

This is a blog application built using Gridsome, a Vue.js Framework. This blog fetches remote data from a CMS and its pages are prerendered on request to encourage Search Engine Indexing.

## Testing 
This blog uses Jest and Vue testing Library for Unit testing.

- [Vue Testing Library](https://github.com/testing-library/vue-testing-library) 


## Development
Clone this repo

### 1. Install Gridsome CLI tool if you don't have

`npm install --global @gridsome/cli`

### 2. Create a Gridsome project

1. `gridsome develop` to start a local dev server at `http://localhost:8080`
2. Happy coding 🎉🙌
